package ex1;

import java.util.Scanner;

public class DigitsSum {
    private int number;
    private int sum = 0;

    public DigitsSum() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number: ");
        number = in.nextInt();
        this.number = number;
    }

    public int numberSum() {
        while (number > 0) {
            sum = sum + number % 10;
            number = number / 10;
        }
        return sum;
    }

    public void checkSum() {
        if (sum % 2 == 0 && sum % 5 == 0)
            System.out.println("Sum can be divided by 2 and 5");
        if (sum % 3 == 0 || sum % 10 == 0)
            System.out.println("Sum can be divided by 3 or 10");
    }
}
