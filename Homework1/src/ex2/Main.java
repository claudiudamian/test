package ex2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int number;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number: ");
        number = in.nextInt();
        System.out.println(number % 2 == 0 ? "The number is even" : "The number is odd");
    }
}
