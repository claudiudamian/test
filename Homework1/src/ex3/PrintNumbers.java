package ex3;

import java.util.Scanner;

public class PrintNumbers {
    private int number;

    public PrintNumbers() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the final number: ");
        number = in.nextInt();
        this.number = number;
    }

    public void printRange() {
        System.out.println("The even numbers from the given range are: ");
        for (int i = 1; i < number; i++) {
            if (i % 2 == 0)
                System.out.println(i);
        }
    }
}
