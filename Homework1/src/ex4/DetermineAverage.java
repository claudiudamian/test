package ex4;

import java.util.Scanner;

public class DetermineAverage {
    private int[] number = new int[3];

    public DetermineAverage() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter 3 numbers: ");
        for (int i = 0; i < number.length; i++) {
            number[i] = in.nextInt();
        }
    }

    public Double calculateAverage() {
        int sum = 0;
        for (int i1 : number) {
            sum = sum + i1;
        }
        Double average;
        average = Double.valueOf(sum / number.length);
        return average;
    }
}
