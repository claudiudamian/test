package ex5;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int first;
        int last;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the first and last numbers of the range: ");
        first = in.nextInt();
        last = in.nextInt();
        System.out.println("The sum of the numbers is " + sumOfOddNumbers(first, last));
    }

    public static int sumOfOddNumbers(int first, int last) {
        int sum = 0;
        while (first <= last) {
            if (first % 2 != 0)
                sum += first;
            first++;
        }
        return sum;
    }
}
