package ex6;

import java.util.Scanner;

public class PrintDay {
    private int day;

    public PrintDay() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number of the day: ");
        day = in.nextInt();
    }

    public void printDay() {
        switch (day) {
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Saturday");
                break;
            case 7:
                System.out.println("Sunday");
                break;
            default:
                System.out.println("The entered number does not correspond to a day.");
        }
    }
}
