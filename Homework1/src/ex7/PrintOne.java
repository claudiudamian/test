package ex7;

import java.util.Scanner;

public class PrintOne {
    private int number;

    public PrintOne() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number of rows and columns: ");
        number = in.nextInt();
    }

    public void displayArray() {
        for (int i = 0; i < number; i++) {
            for (int j = 0; j < number; j++) {
                System.out.print("1 ");
            }
            System.out.println();
        }
    }
}
