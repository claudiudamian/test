package ex8;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int number;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number you want to calculate factorial for: ");
        number = in.nextInt();
        System.out.println("The factorial of " + number + " is: " + calculateFactorial(number));
    }

    public static int calculateFactorial(int number) {
        int factorial = 1;
        for (int i = 1; i <= number; i++) {
            factorial *= i;
        }
        return factorial;
    }
}
