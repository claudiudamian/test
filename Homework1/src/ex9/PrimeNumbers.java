package ex9;

import java.util.Scanner;

public class PrimeNumbers {
    int number;

    public PrimeNumbers() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number: ");
        number = in.nextInt();
    }

    public void isPrime() {
        boolean flag = false;
        int i = 2;
        while (i <= number / 2) {
            if (number % i == 0) {
                flag = true;
                break;
            }
            ++i;
        }
        if (!flag && number != 0 && number != 1)
            System.out.println(number + " is prime.");
        else
            System.out.println(number + " is not prime.");
    }
}
